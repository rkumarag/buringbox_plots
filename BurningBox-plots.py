import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import sys

# Read the CSV file
file_path = '/home/rahulagrawal054/cleanroom/BurningBox/20240410_111128/20240410_111128.csv'
df = pd.read_csv(file_path)

# Remove leading and trailing spaces from column names
df.columns = df.columns.str.strip()

# Convert #DATETIME column to datetime format
df['#DATETIME'] = pd.to_datetime(df['#DATETIME'], format='%Y%m%d_%H%M%S')

# Extract date from #DATETIME column
date = df['#DATETIME'].dt.strftime('%d %b %Y').iloc[0]

# Extract time from #DATETIME column and convert it to string
df['Time'] = df['#DATETIME'].dt.strftime('%H:%M')

# Plotting the 1st graph  for read-temp and read-dewpoint
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['read-temp'], color='black', label='temp')
plt.plot(df.index, df['read-dewpoint'], color='blue', label='dewpoint')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/read_temp and read_dew_point.png')
plt.show()


# Plotting the 2nd graph for read-temp
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['read-temp'], color='black', label='temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/read_temp.png')
plt.show()

# Plotting the 3rd graph for read-dewpoint
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['read-dewpoint'], color='blue', label='dewpoint')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/read_dew_point.png')
plt.show()

#Plotting the 4th graph for carrier1 and carrier2
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['carrier1'], color='blue', label='carrier1 temp')
plt.plot(df.index, df['carrier2'], color='orange', label='carrier2 temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/carrier1 and carrier2.png')
plt.show()

#Plotting the  5th graph for carrier1
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['carrier1'], color='blue', label='carrier1 temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/carrier1')
plt.show()

#Plotting the  6th graph for carrier2
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', alpha=0.7, label='set-temp')
plt.plot(df.index, df['carrier2'], color='orange', label='carrier2 temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/carrier2.png')
plt.show()

# Plotting the  7th  graph (rtd1, rtd2, rtd3)
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', label='set-temp')
plt.plot(df.index, df['rtd1'], color='lightblue', label=' right frame temp')
plt.plot(df.index, df['rtd2'], color='lightgreen', label='left frame temp')
plt.plot(df.index, df['rtd3'], color='lightcoral', label='ambient temp in frame')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/left-right-ambient_frame_temp.png')
plt.show()


# Plotting the  8th  graph (rtd1)
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', label='set-temp')
plt.plot(df.index, df['rtd1'], color='lightblue', label='left frame temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/left_frame_temp.png')
plt.show()

# Plotting the  9th  graph (rtd2)
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', label='set-temp')
plt.plot(df.index, df['rtd2'], color='lightgreen', label='right frame temp')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/right_frame_temp.png')
plt.show()


# Plotting the  10th  graph (rtd3)
plt.figure(figsize=(10, 6))
plt.plot(df.index, df['set-temp'], color='black', linestyle='--', label='set-temp')
plt.plot(df.index, df['rtd3'], color='lightcoral', label='ambient temp in frame')
#plt.title(f'Temperature Readings on {date}')
plt.title(f'Temperature Readings')
plt.xlabel('Time')
plt.ylabel('Temperature (°C)')
plt.ylim(-50, 30)
plt.legend(loc='lower right')
#plt.axvline(x=178, color='r', linestyle=':', label='30 min gap')  # Marking parallel line
#plt.text(178, 15,'30 mins break', verticalalignment='bottom', horizontalalignment='right', color='r', rotation=90)
plt.xticks(df.index[::20], df['Time'][::20], rotation=90)
plt.grid(True)
plt.tight_layout()
plt.savefig('/home/rahulagrawal054/cleanroom/BurningBox/Plots/20240410_111128_plots/ambient_frame_temp.png')
plt.show()


